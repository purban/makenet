﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MakeNet.Parser
{
    public class Commands
    {
        private Dictionary<string, CommandProcessor> commands = new Dictionary<string, CommandProcessor>();
        public delegate bool CommandProcessor(string options, out string result, List<string> content, int lineNumber);
        string currentPath;
        string tempDirectoryPath;
        Makefile parsedMakefile;
        Rules rules;

        public Commands(string currentPath, string tempDirectoryPath, Makefile parsedMakefile, Rules rules)
        {
            this.currentPath = currentPath;
            this.tempDirectoryPath = tempDirectoryPath;
            this.parsedMakefile = parsedMakefile;
            this.rules = rules;

            RegisterCommand("info", CmdInfo);
            RegisterCommand("wildcard", CmdWildcard);
            RegisterCommand("firstword", CmdFirstword);
            RegisterCommand("error", CmdError);
            RegisterCommand("sort", CmdSort);
            RegisterCommand("dir", CmdDir);
            RegisterCommand("patsubst", CmdPatsubst);
            RegisterCommand("findstring", CmdFindstring);
            RegisterCommand("strip", CmdStrip);
            RegisterCommand("suffix", CmdSuffix);
            RegisterCommand("call", CmdCall);
            RegisterCommand("addsuffix", CmdAddsuffix);
            RegisterCommand("shell", CmdShell);
            RegisterCommand("subst", CmdSubst);
            RegisterCommand("filter-out", CmdFilterOut);
            RegisterCommand("eval", CmdEval);
            RegisterCommand("echo", CmdEcho);
            RegisterCommand("rm", CmdRm);
            RegisterCommand("cp", CmdCopy);
            RegisterCommand("if", CmdIf);
            RegisterCommand("concat", CmdConcat);
            RegisterCommand("textreplace", CmdTextReplace);
            RegisterCommand("oneheader", CmdOneHeader);
            RegisterCommand("words", CmdWords);
            RegisterCommand("__MAKE__", CmdMake);

            // Build in shell commands
            //------------------------
            RegisterCommand("find", ShellCmdFind);
        }

        public bool ProcessCommand(string commandName, string options, bool directOutput, out string result)
        {
            result = "";

            // Handle special command MAKE to add exported variables
            //------------------------------------------------------

            if (options.StartsWith("\"") && options.EndsWith("\""))
            {
                options = options.Substring(1, options.Length - 2);
            }

            // Replace tabs with space
            //------------------------
            options = options.Replace("\t", " ");

            if (commands.ContainsKey(commandName))
            {
                List<string> content = null;
                if (ProcessCommand(commandName, options, out result, content, 0))
                {
                    return true;
                }
            }
            else
            {
                // Remove output redirection
                //--------------------------
                if (options.Contains(">/dev/null"))
                {
                    directOutput = false;
                    options = options.Replace(">/dev/null", "");
                }

                // Detect file redirect
                //---------------------
                string outputFile = null;
                bool append = false;
                string[] parts = options.Split(new char[] { '>' });

                if (parts.Length > 1)
                {
                    foreach (string part in parts.Skip(1))
                    {
                        outputFile += part;
                    }
                    outputFile = outputFile.Trim();

                    if (options.Contains(">>"))
                    {
                        append = true;
                    }

                    options = parts[0];
                    directOutput = false;

                    Debug.WriteLine("Detect file redirect for command '" + commandName + "': '" + options + "' in file '" + outputFile + "' Append(" + append + ")");
                }

                // Try to execute external application
                //------------------------------------
                List<string> resultList;
                bool error = Utils.ExecuteCmdProcess(currentPath, commandName, options, directOutput, out resultList);
                foreach(string resultLine in resultList)
                {
                    result += resultLine + "\n";
                }

                // Redirect output
                //----------------
                if (outputFile != null)
                {
                    if (append)
                    {
                        File.AppendAllText(outputFile, result);
                    }
                    else
                    {
                        File.WriteAllText(outputFile, result);
                    }

                    result = "";
                }

                return error;
            }

            return false;
        }

        public bool ProcessCommand(string commandName, string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            // Some replacements
            //------------------
            if (options != null)
            {
                // Replace tabs with space
                options = options.Replace("\t", " ");

                // Replace escaped #-Characters
                options = options.Replace("\\#", "#");
            }

            CommandProcessor cmdProcessor;
            if (commands.TryGetValue(commandName, out cmdProcessor))
            {
                if (options != null)
                {
                    options = options.Trim();
                }
                if (cmdProcessor(options, out result, content, lineNumber))
                {
                    return true;
                }
                return false;

            }
            else
            {
                Console.WriteLine("Unknown command '" + commandName + "'");
                return false;
            }
        }

        public bool ProcessCommands(string line, out string lineToStore, List<string> content, int lineNumber)
        {
            lineToStore = null;
            if (line == null) return true;

            // Skip function line
            //-------------------
            if (rules.LineIsVariableDeclaration(line) && !rules.LineIsExecutingVariableDeclaration(line))
            {
                lineToStore = line;
                return true;
            }

            // Skipt if line starts with tab (target indicator)
            //-------------------------------------------------
            if(line.StartsWith("\t") && !rules.LineIsExecutingVariableDeclaration(line))
            {
                lineToStore = line;
                return true;
            }

            // Process all foreach first
            // $(foreach var,list,text)
            //--------------------------
            int foreachIndex = -1;
            do
            {
                foreachIndex = line.Substring(foreachIndex + 1).IndexOf("$(foreach ");
                if (foreachIndex > -1)
                {
                    int lineIndex = 0;
                    int openBrackets = 0;
                    int currentActiveOption = 0; // 0 == var, 1 == list, 2 == text
                    int currentStartIndex = foreachIndex + "$(foreach ".Length;
                    string var = null;
                    string list = null;
                    string text = null;
                    for (lineIndex = currentStartIndex;
                        lineIndex < line.Length;
                        lineIndex++)
                    {
                        if (line[lineIndex] == ',' && openBrackets == 0)
                        {
                            currentActiveOption++;

                            switch (currentActiveOption)
                            {
                                // var
                                case 1:
                                    var = line.Substring(currentStartIndex, lineIndex - currentStartIndex);
                                    currentStartIndex = lineIndex;
                                    break;
                                // list
                                case 2:
                                    list = line.Substring(currentStartIndex + 1, lineIndex - currentStartIndex - 1);
                                    int listOriginalLength = list.Length;
                                    list = list.Trim();

                                    // Process commands in list part
                                    //------------------------------
                                    if (!ProcessCommands(list, out list, content, lineNumber))
                                    {
                                        // Command stopped execution
                                        return false;
                                    }
                                    line = line.Substring(0, currentStartIndex + 1) + list + line.Substring(lineIndex);
                                    int listLengthDiff = listOriginalLength - list.Length;
                                    lineIndex -= listLengthDiff;

                                    currentStartIndex = lineIndex;
                                    break;
                            }
                        }

                        if (line[lineIndex] == '(')
                        {
                            openBrackets++;
                        }
                        else if (line[lineIndex] == ')')
                        {
                            if (currentActiveOption == 2 && openBrackets == 0)
                            {
                                break;
                            }
                            openBrackets--;
                        }
                    }
                    int textStartIndex = currentStartIndex + 1;
                    int textEndIndex = lineIndex;
                    text = line.Substring(textStartIndex, textEndIndex - currentStartIndex - 1);
                    text = text.Trim();

                    string[] listParts = list.Split(' ');
                    string completeForeachResult = "";
                    foreach (string listPart in listParts)
                    {
                        string part = listPart.Trim();
                        if (part == "") continue;
                        string result;
                        parsedMakefile.SetVariable(var, part);
                        rules.ReplaceLineVariables(text, out result);

                        completeForeachResult += result + " ";
                    }
                    completeForeachResult = completeForeachResult.Trim();

                    line = line.Substring(0, foreachIndex) + completeForeachResult + line.Substring(textEndIndex + 1);
                }
            } while (foreachIndex != -1 && foreachIndex < line.Length);

            int lastCheckedStartChar = line.LastIndexOf("$(");
            int firstStartChar = line.IndexOf("$(");
            while (lastCheckedStartChar >= firstStartChar && firstStartChar != -1)
            {
                string blockContent;
                int blockStart = lastCheckedStartChar + "$(".Length;
                int blockEnd = -1;

                // Get content of $() block
                //-------------------------
                int openBraketCount = 1;
                for (int charIndex = blockStart; charIndex < line.Length; charIndex++)
                {
                    char currentChar = line[charIndex];

                    if (currentChar == '(')
                    {
                        openBraketCount++;
                    }
                    if (currentChar == ')')
                    {
                        openBraketCount--;
                        blockEnd = charIndex - 1;
                        if (openBraketCount == 0)
                        {
                            break;
                        }
                    }
                }
                blockContent = line.Substring(blockStart, blockEnd - blockStart + 1);

                // Check if we match variable usage instead of command call
                // --> must contain whitespace
                //----------------------------------------------------------
                string[] commandParts = blockContent.Split(new char[] { ' ' }, 2);
                if (commandParts.Length == 2)
                {
                    string command = commandParts[0];
                    string arguments = commandParts[1];
                    string result;

                    if (!ProcessCommand(command, arguments, out result, content, lineNumber))
                    {
                        // Command stopped execution
                        return false;
                    }

                    // Replace result in line
                    //-----------------------
                    line = line.Substring(0, lastCheckedStartChar) + result + line.Substring(lastCheckedStartChar + blockContent.Length + "$()".Length);
                }

                if (lastCheckedStartChar == 0)
                {
                    lastCheckedStartChar = -1;
                }
                else
                {
                    string previousLinePart = line.Substring(0, lastCheckedStartChar - 1);
                    lastCheckedStartChar = previousLinePart.LastIndexOf("$(");
                }
            }

            lineToStore = line;
            return true;
        }

        public void RegisterCommand(string name, CommandProcessor cmdProcessor)
        {
            commands.Add(name, cmdProcessor);
        }

        private bool CmdInfo(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return false;
            }
            Console.WriteLine(options);
            return true;
        }
        private bool CmdWildcardFolders(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";

            string path;
            string option = "";
            string totalPath;

            if(options.Contains("*"))
            {
                string[] optionParts = options.Split(new char[] { '*' }, 2);
                path = Path.GetDirectoryName(optionParts[0]);
                option = optionParts[1];

                if (option.EndsWith("/") || option.EndsWith("\\"))
                {
                    option = option.Substring(0, option.Length - 1);
                }
            }
            else
            {
                path = Path.GetDirectoryName(options);
            }

            totalPath = Path.Combine(currentPath, path);

            // Check if direcotry exists
            //--------------------------
            if (!Directory.Exists(totalPath))
            {
                return true;
            }

            string[] allFolders = Directory.GetDirectories(totalPath, "*.*", SearchOption.AllDirectories);
            foreach (string foundPath in allFolders)
            {
                string reducedPath = foundPath.Substring(totalPath.Length);
                string reducedPathSlashes = reducedPath.Replace("\\", "/");
                string reducedPathBackslashes = reducedPath.Replace("/", "\\");

                if(option.Length != 0)
                {
                    if (reducedPathSlashes.EndsWith(option) || reducedPathBackslashes.EndsWith(option))
                    {
                        result += Utils.GetRelativePath(currentPath, foundPath) + "\\ ";
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                result += Utils.GetRelativePath(currentPath, foundPath) + "\\ ";
            }

            result = result.Trim();

            return true;
        }

        private bool CmdWildcard(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";

            string path;
            string option;
            string totalPath;

            options = Utils.GetNormalizedPath(options);

            path = Path.GetDirectoryName(options);
            option = Path.GetFileName(options);
            totalPath = Path.Combine(currentPath, path);

            // Check if path contains wildcard character
            // then only folders names will be searched
            //------------------------------------------
            if (path.Contains("*"))
            {
                return CmdWildcardFolders(options, out result, content, lineNumber);
            }

            // Check if direcotry exists
            //--------------------------
            if (!Directory.Exists(totalPath))
            {
                return true;
            }

            List<string> allFilesFolders = new List<string>();
            allFilesFolders.AddRange(Directory.GetFiles(totalPath));
            allFilesFolders.AddRange(Directory.GetDirectories(totalPath));
            foreach (string filePath in allFilesFolders)
            {
                string fileName = Path.GetFileName(filePath);

                if (option.StartsWith("*"))
                {
                    // Check for multiple catch syntax like '*.[ao]'
                    if (option.Contains(".["))
                    {
                        bool match = false;
                        foreach (char fileEnding in option.Substring(3))
                        {
                            if (fileName.EndsWith(fileEnding.ToString()))
                            {
                                match = true;
                                break;
                            }
                        }
                        if(!match)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (!fileName.EndsWith(option.Substring(1)))
                        {
                            continue;
                        }
                    }
                }
                else if (option.EndsWith("*"))
                {
                    if (!fileName.StartsWith(option.Substring(0, option.Length - 1)))
                    {
                        continue;
                    }
                }
                else if(option.Length != 0)
                {
                    if (fileName != option)
                    {
                        continue;
                    }
                }

                result += Path.Combine(path, fileName) + " ";
            }

            result = result.Trim();

            return true;
        }

        private bool CmdFirstword(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            string[] words = options.Split(' ');
            if (words.Length > 0)
            {
                result = words[0].Trim();
            }

            return true;
        }

        private bool CmdError(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                options = "No error description";
            }

            Console.WriteLine("Error: " + options);

            return false;
        }

        private bool CmdSort(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            string[] words = options.Split(' ');
            List<string> sortedWords = new List<string>(words);

            sortedWords = sortedWords.Distinct().ToList(); // Filter duplicates
            sortedWords.Sort(); // Sort

            result = string.Join(" ", sortedWords);

            return true;
        }

        private bool CmdDir(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";
            string[] words = options.Split(' ');
            foreach (string word in words)
            {
                string directoryName = Path.GetDirectoryName(word);
                if (directoryName == "")
                {
                    directoryName = "./";
                }
                result += directoryName + " ";
            }

            result = result.Trim();

            return true;
        }

        private bool CmdPatsubst(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";
            string[] args = options.Split(new char[] { ',' }, 3);
            if (args.Length != 3)
            {
                Console.WriteLine("Too less arguments for patsubst command");
                return false;
            }

            string pattern = args[0].Trim();
            string replace = args[1].Trim();
            string wordList = args[2].Trim();

            result = "";
            string[] words = wordList.Split(' ');
            foreach (string word in words)
            {
                string newWord = null;
                string match = null;
                string trimmedWord = word.Trim();

                if(trimmedWord.Length == 0)
                {
                    continue;
                }

                // Get matching part
                //------------------
                if (pattern.StartsWith("%"))
                {
                    if (trimmedWord.EndsWith(pattern.Substring(1)))
                    {
                        match = trimmedWord.Substring(0, trimmedWord.Length - pattern.Substring(1).Length);
                    }
                }
                else if (pattern.EndsWith("%"))
                {
                    if (trimmedWord.StartsWith(pattern.Substring(-1)))
                    {
                        match = trimmedWord.Substring(pattern.Length - 1);
                    }
                }
                else
                {
                    if (pattern == trimmedWord)
                    {
                        match = trimmedWord;
                    }
                }

                // Replace match with replace
                //---------------------------
                if (match != null)
                {
                    if (replace.StartsWith("%"))
                    {
                        newWord = match + replace.Substring(1);
                    }
                    else if (replace.EndsWith("%"))
                    {
                        newWord = replace.Substring(0, replace.Length - 1) + match;
                    }
                    else
                    {
                        newWord = replace;
                    }
                }
                else
                {
                    newWord = trimmedWord;
                }

                if (newWord != null)
                {
                    result += newWord + " ";
                }
            }

            return true;
        }

        private bool CmdFindstring(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            string[] args = options.Split(new char[] { ',' }, 2);
            if (args.Length != 2)
            {
                Console.WriteLine("Too less arguments for findstring command");
                return false;
            }

            string find = args[0].Trim();
            string wordList = args[1].Trim();
            string[] words = wordList.Split(' ');
            foreach (string word in words)
            {
                if (word.Contains(find))
                {
                    result = find;
                    break;
                }
            }

            return true;
        }

        private bool CmdStrip(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";
            string[] args = options.Split(new char[] { ' ' });
            foreach(string arg in args)
            {
                string argTrimmed = arg.Trim();
                if (argTrimmed.Length == 0) continue;

                result += argTrimmed;
            }

            return true;
        }

        private bool CmdSuffix(string options, out string result, List<string> content, int lineNumber)
        {
            result = "";

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";
            string[] args = options.Split(new char[] { ' ' });
            foreach (string arg in args)
            {
                string argTrimmed = arg.Trim();
                if (argTrimmed.Length == 0) continue;

                if (!argTrimmed.Contains(".")) continue;
                if (argTrimmed.EndsWith(".")) continue;

                int lastIndex = argTrimmed.LastIndexOf(".");
                result += argTrimmed.Substring(lastIndex + 1, argTrimmed.Length - (lastIndex + 1));
            }

            return true;
        }

        private bool CmdCall(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            string[] args = options.Split(new char[] { ',' });

            string functionName = args[0].Trim();

            // Try to find Macro
            //------------------
            Macro macro = parsedMakefile.GetMacro(functionName);
            if (macro != null)
            {
                result = "";
                foreach(string line in macro.Content)
                {
                    string processedLine = line;

                    for (int argNumber = 1; argNumber < args.Length; argNumber++)
                    {
                        processedLine = processedLine.Replace("$" + argNumber, args[argNumber]);
                        processedLine = processedLine.Replace("$(" + argNumber + ")", args[argNumber]);
                    }

                    result += processedLine + "\r\n";
                }

                return true;
            }

            string functionContent = parsedMakefile.GetVariable(functionName);

            if (functionContent == null)
            {
                Console.WriteLine("Unknown function '" + functionName + "'");
                return false;
            }

            for (int argNumber = 1; argNumber < args.Length; argNumber++)
            {
                functionContent = functionContent.Replace("$" + argNumber, args[argNumber]);
                functionContent = functionContent.Replace("$(" + argNumber + ")", args[argNumber]);
            }

            result = functionContent;

            // Process commands inside new line
            //---------------------------------
            if (!ProcessCommands(result, out result, content, lineNumber))
            {
                return false;
            }

            return true;
        }

        private bool CmdAddsuffix(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            string[] args = options.Split(new char[] { ',' }, 2);
            if (args.Length != 2)
            {
                Console.WriteLine("Too less arguments for addsuffix command");
                return false;
            }

            string suffix = args[0].Trim();
            string wordList = args[1].Trim();
            string[] words = wordList.Split(' ');
            result = "";
            foreach (string word in words)
            {
                result += word + suffix + " ";
            }

            return true;
        }

        private bool CmdEcho(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;
            string outputFile = null;
            bool append = false;
            string[] parts = options.Split(new char[] { '>' });

            if(parts.Length > 1)
            {
                foreach(string part in parts.Skip(1))
                {
                    outputFile += part;
                }
                outputFile = outputFile.Trim();

                if (options.Contains(">>"))
                {
                    append = true;
                }

                options = parts[0];

                Debug.WriteLine("Echo '" + options + "' in file '" + outputFile + "' Append(" + append + ")");
            }

            if(options.StartsWith("\"") && options.EndsWith("\""))
            {
                options = options.Substring(1);
                options = options.Substring(0, options.Length - 1);
            }

            if(outputFile != null)
            {
                if(append)
                {
                    File.AppendAllText(outputFile, options);
                }
                else
                {
                    File.WriteAllText(outputFile, options);
                }
            }
            else
            {
                Console.WriteLine(options);
            }

            return true;
        }

        private bool CmdIf(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            string[] lines = options.Split(new char[] { ';' });

            bool ifStatementResult = false;
            for(int lineIndex = 0; lineIndex < lines.Length; lineIndex++)
            {
                string line = lines[lineIndex].Trim();

                // Check if-argument
                //------------------
                if (lineIndex == 0)
                {
                    string[] orConditions = line.Split(new string[] { "||" }, StringSplitOptions.None);
                    foreach(string orCondition in orConditions)
                    {
                        bool andConditionResult = false;

                        string[] andConditions = line.Split(new string[] { "&&" }, StringSplitOptions.None);
                        foreach(string andCondition in andConditions)
                        {
                            string cleanCondition = andCondition.Replace("[", "").Replace("]", "").Replace("\"", "").Replace("/", "\\").Trim();
                            
                            andConditionResult = false;

                            // ==
                            if (cleanCondition.Contains("=="))
                            {
                                string[] conditionParts = cleanCondition.Split(new string[] { "==" }, StringSplitOptions.None);
                                if(conditionParts.Length == 2)
                                {
                                    string firstArg = conditionParts[0].Trim();
                                    string secondArg = conditionParts[1].Trim();

                                    if (secondArg.StartsWith("*"))
                                    {
                                        if (secondArg.EndsWith("*"))
                                        {
                                            if(firstArg.Contains(secondArg.Substring(1, secondArg.Length - 2)))
                                            {
                                                andConditionResult = true;
                                            }
                                        }
                                        else
                                        {
                                            if (firstArg.EndsWith(secondArg.Substring(1)))
                                            {
                                                andConditionResult = true;
                                            }
                                        }
                                    }
                                    else if(secondArg.EndsWith("*"))
                                    {
                                        if (firstArg.StartsWith(secondArg.Substring(0, secondArg.Length - 1)))
                                        {
                                            andConditionResult = true;
                                        }
                                    }
                                }
                            }

                            // !=
                            if(cleanCondition.Contains("!="))
                            {
                                string[] conditionParts = cleanCondition.Split(new string[] { "!=" }, StringSplitOptions.None);
                                if (conditionParts.Length == 2)
                                {
                                    string firstArg = conditionParts[0].Trim();
                                    string secondArg = conditionParts[1].Trim();

                                    if (secondArg.StartsWith("*"))
                                    {
                                        if (secondArg.EndsWith("*"))
                                        {
                                            if (!firstArg.Contains(secondArg.Substring(1, secondArg.Length - 2)))
                                            {
                                                andConditionResult = true;
                                            }
                                        }
                                        else
                                        {
                                            if (!firstArg.EndsWith(secondArg.Substring(1)))
                                            {
                                                andConditionResult = true;
                                            }
                                        }
                                    }
                                    else if (secondArg.EndsWith("*"))
                                    {
                                        if (!firstArg.StartsWith(secondArg.Substring(0, secondArg.Length - 1)))
                                        {
                                            andConditionResult = true;
                                        }
                                    }
                                }
                            }

                            // Break if any '&&' condition failed
                            //-----------------------------------
                            if(!andConditionResult)
                            {
                                break;
                            }
                        }

                        if(andConditionResult)
                        {
                            ifStatementResult = true;
                            break;
                        }
                    }
                }
                // Execute if true
                //----------------
                else if(line.StartsWith("then") && ifStatementResult)
                {
                    string command = null;
                    string arguments = null;
                    string[] parts = line.Substring("then".Length).Trim().Split(new char[] { ' ' }, 2);
                    if (parts.Length >= 2)
                    {
                        command = parts[0];
                        arguments = parts[1];
                    }

                    if (command != null)
                    {
                        string commandResult;
                        ProcessCommand(command, arguments, true, out commandResult);
                    }
                }
                // Execute if false
                //-----------------
                else if(line.StartsWith("else") && !ifStatementResult)
                {
                    string command = null;
                    string arguments = null;
                    string[] parts = line.Substring("else".Length).Trim().Split(new char[] { ' ' }, 2);
                    if (parts.Length >= 2)
                    {
                        command = parts[0];
                        arguments = parts[1];
                    }

                    if (command != null)
                    {
                        string commandResult;
                        ProcessCommand(command, arguments, true, out commandResult);
                    }
                }
                // End
                //----
                else if(line.StartsWith("fi"))
                {
                    break;
                }
            }

            return true;
        }

        private bool CmdRm(string options, out string result, List<string> content, int lineNumber)
        {
            result = "";
            bool force = false;
            bool recursive = false;

            // Convert relative paths
            //-----------------------
            if (options.StartsWith("./"))
            {
                options = options.Substring("./".Length);
            }
            options = options.Replace(" ./", " ");

            // Handle common options.
            // This is ugly but we just want to support basic 'rm' usage
            //----------------------------------------------------------
            if (options.StartsWith("-f"))
            {
                force = true;
                options = options.Substring("-f".Length).TrimStart();
            }

            if (options.StartsWith("-rf"))
            {
                force = true;
                recursive = true;
                options = options.Substring("-rf".Length).TrimStart();
            }

            // Replace slashes with backslashes
            //---------------------------------
            options = options.Replace("/", "\\");

            string[] splittedOptions = options.Split(new char[] { ' ' });
            List<string> argumentLists = new List<string>();

            string argumentList = "";
            foreach(string option in splittedOptions)
            {
                argumentList += option + " ";

                if(argumentList.Length > 2048)
                {
                    argumentLists.Add(argumentList);
                    argumentList = "";
                }
            }

            // Add last
            if(argumentList.Length > 0)
            {
                argumentLists.Add(argumentList);
            }

            List<string> resultList = new List<string>();
            foreach (string arguments in argumentLists)
            {
                List<string> processResult;

                // Call equivalent windows functions to do the job
                //------------------------------------------------
                if(!Utils.ExecuteCmdProcess(currentPath, "del", ((force) ? "/f /q " : "") + ((recursive) ? "/s " : "") + arguments, false, out processResult))
                {
                    return false;
                }

                resultList.AddRange(processResult);
            }

            foreach(string line in resultList)
            {
                result += line + "\r\n";
            }

            return true;
        }

        private bool CmdCopy(string options, out string result, List<string> content, int lineNumber)
        {
            result = "";

            // Convert relative paths
            //-----------------------
            if(options.StartsWith("./"))
            {
                options = options.Substring("./".Length);
            }
            options = options.Replace(" ./", " ");

            List<string> resultList;
            if (!Utils.ExecuteCmdProcess(currentPath, "copy", "/y " + options, false, out resultList))
            {
                return false;
            }

            return true;
        }

        private bool CmdShell(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            string[] optionParts = options.Trim().Split(' ');

            if (optionParts.Length == 0)
            {
                Console.WriteLine("Invalid shell command");
                return false;
            }

            string shellCommand = optionParts[0].Trim();
            bool ignoreResult = false;
            bool silent = false;
            if (shellCommand.StartsWith("@"))
            {
                silent = true;
                shellCommand = shellCommand.Substring(1);
            }
            if (shellCommand.StartsWith("-"))
            {
                ignoreResult = true;
                shellCommand = shellCommand.Substring(1);
            }
            List<string> shellOptions = new List<string>(optionParts.Skip(1).ToArray());
            string shellOptionsString = string.Join(" ", shellOptions);

            if (commands.ContainsKey(shellCommand))
            {
                bool error = ProcessCommand(shellCommand, shellOptionsString, out result, content, lineNumber);
                if(!ignoreResult)
                {
                    return error;
                }
            }
            else
            {
                List<string> resultOutput;
                if(!Utils.ExecuteProcess(currentPath, shellCommand, shellOptionsString, !silent, out resultOutput) && !ignoreResult)
                {
                    return false;
                }
            }

            return true;
        }

        private bool CmdSubst(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";
            string[] args = options.Split(new char[] { ',' }, 3);
            if (args.Length != 3)
            {
                Console.WriteLine("Too less arguments for subst command");
                return false;
            }

            string from = args[0].Trim();
            string to = args[1].Trim();
            string wordList = args[2].Trim();

            if ((from == "/" || from == "\\") && (to == "/" || to == "\\"))
            {
                // Should work perfect if both arguments are slashes
            }
            else
            {
                from = from.Replace("/", "\\");
                to = to.Replace("/", "\\");
                wordList = wordList.Replace("/", "\\");
            }

            // Special Make behavior:
            // Trailing slash will be ignored
            //-------------------------------
            if(args[0].Trim().EndsWith("/"))
            {
                wordList = wordList.Replace(from.Substring(0, from.Length-1), to);
            }

            result = wordList.Replace(from, to);

            return true;
        }

        private bool CmdFilterOut(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if (options == null || options.Length == 0)
            {
                return true;
            }

            result = "";
            string[] args = options.Split(new char[] { ',' }, 2);
            if (args.Length != 2)
            {
                Console.WriteLine("Too less arguments for filter-out command");
                return false;
            }

            string pattern = Utils.GetRelativePath(currentPath, args[0].Trim()).Replace("\\", "/");
            string wildcardPatternBack = pattern.Substring(0, pattern.Length - 1);
            string wildcardPatternFront = pattern.Substring(1);
            string wordList = args[1].Trim();
            string[] words = wordList.Split(' ');
            result = "";
            foreach (string word in words)
            {
                string fixedWord = Utils.GetRelativePath(currentPath, word).Replace("\\", "/");

                //Debug.WriteLine("Filter-Out: Word(" + word + ") FixedWord(" + fixedWord + ") Pattern(" + pattern + ")");

                if(fixedWord == pattern)
                {
                    continue;
                }

                if(pattern.EndsWith("%"))
                {
                    if(fixedWord.StartsWith(wildcardPatternBack))
                    {
                        //Debug.WriteLine("Filter-Out Match!");
                        continue;
                    }
                }

                if (pattern.StartsWith("%"))
                {
                    if (fixedWord.StartsWith(wildcardPatternFront))
                    {
                        continue;
                    }
                }

                result += word + " ";
            }

            return true;
        }

        private bool CmdMake(string options, out string result, List<string> content, int lineNumber)
        {
            // Handle exported variables
            //--------------------------
            foreach(string exportedVariable in parsedMakefile.ExportedVariables)
            {
                string variable = parsedMakefile.GetVariable(exportedVariable);
                options += " " + exportedVariable + "=" + variable;
            }
            options += " -j" + BuildParameters.parallelJobCount;
            options += " -z"; // Internal call

            return ProcessCommand(System.AppDomain.CurrentDomain.FriendlyName, options, true, out result);
        }

        private bool CmdEval(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;

            if(content == null)
            {
                return false;
            }

            if(options == null)
            {
                return true;
            }

            string[] lines = Regex.Split(options, "\r\n");

            // Clear new input file
            //---------------------
            List<string> includedInput = rules.ClearInput(lines);

            // Remove line with incldue statement
            //-----------------------------------
            content.RemoveAt(lineNumber);

            // Copy new lines into existing content
            //-------------------------------------
            content.InsertRange(lineNumber, includedInput);

            // Add dummy line which will be removed by caller
            // to match normal rule behavior
            //-----------------------------------------------
            content.Insert(lineNumber, "DUMMY");

            return true;
        }

        private bool CmdConcat(string options, out string result, List<string> content, int lineNumber)
        {
            result = "";

            string outputFile = null;
            string[] parts = options.Split(new char[] { '>' });
            if(parts.Length > 1)
            {
                outputFile = parts[1].Trim();
            }

            string inputFile = parts[0].Trim();
            if(!File.Exists(inputFile))
            {
                return false;
            }

            Debug.WriteLine("Concat: InputFile(" + inputFile + ") OutputFile(" + outputFile + ")");

            File.WriteAllText(outputFile, "");

            foreach (string line in File.ReadLines(inputFile))
            {
                string processedLine = line;
                
                if(processedLine.EndsWith("\\"))
                {
                    processedLine = processedLine.Substring(0, line.Length - 1);
                }
                processedLine = processedLine.Trim();

                if(!File.Exists(processedLine))
                {
                    Debug.WriteLine("Concat: File not found: '" + processedLine + "'");
                    continue;
                }

                string inputFileContent = File.ReadAllText(processedLine);

                if(outputFile != null)
                {
                    File.AppendAllText(outputFile, inputFileContent);
                }
                else
                {
                    Console.Write(inputFileContent);
                }
            }

            return true;
        }

        private bool CmdOneHeader(string options, out string result, List<string> content, int lineNumber)
        {
            result = null;
            string outputFile = null;
            bool append = false;
            string[] parts = options.Split(new char[] { '>' });

            if (parts.Length > 1)
            {
                foreach (string part in parts.Skip(1))
                {
                    outputFile += part;
                }
                outputFile = outputFile.Trim();

                if (options.Contains(">>"))
                {
                    append = true;
                }

                options = parts[0];

                Debug.WriteLine("OneHeader '" + options + "' in file '" + outputFile + "' Append(" + append + ")");
            }

            // Collect all headers and copy them together
            //-------------------------------------------
            string output = "";
            
            string inputFile = options.Trim();
            if(!File.Exists(inputFile))
            {
                Debug.WriteLine("OneHeader: InputFile does not exist '" + inputFile + "'");
                return false;
            }

            Debug.WriteLine("OneHeader not implemented!");
            return false;

            //output = File.ReadAllText(inputFile);
            //output = output.Replace("\r\r\n", "\r\n");

            /*
            List<string> headers = new List<string>();
            bool fileLine = false;
            string prevLine = null;
            foreach (string line in File.ReadLines(inputFile))
            {
                if(line.Trim().Length == 0)
                {
                    continue;
                }

                if(line.StartsWith("# "))
                {
                    prevLine = line;
                    fileLine = true;
                }
                else
                {
                    if(fileLine)
                    {
                        fileLine = false;
                        int firstQuote = prevLine.IndexOf("\"");
                        if(firstQuote == -1)
                        {
                            Debug.WriteLine("OneHeader: Line does not contain start quote: '" + prevLine + "'");
                            continue;
                        }
                        prevLine = prevLine.Substring(firstQuote + 1);
                        int lastQuote = prevLine.IndexOf("\"");
                        if(lastQuote == -1)
                        {
                            Debug.WriteLine("OneHeader: Line does not contain end quote: '" + prevLine + "'");
                            continue;
                        }
                        prevLine = prevLine.Substring(0, lastQuote);
                        headers.Add(prevLine);
                    }
                }
            }

            Debug.WriteLine("OneHeader: Found " + headers.Count + " entries");

            foreach(string line in headers.Distinct())
            {
                output += line + Environment.NewLine;
            }*/

            /*
            // Count dots and save headers sorted in list
            SortedDictionary<int, List<string>> headers = new SortedDictionary<int, List<string>>();
            int fileCount = 0;
            foreach (string line in File.ReadLines(inputFile))
            {
                // Skipt lines which does not start with a dot
                if (!line.StartsWith("."))
                {
                    continue;
                }

                // Count dots at start of line
                int dotCount = 0;
                foreach(char c in line)
                {
                    if(c != '.')
                    {
                        break;
                    }
                    dotCount++;
                }

                string processedLine = line.Substring(dotCount).Trim();

                if(!headers.ContainsKey(dotCount))
                {
                    headers.Add(dotCount, new List<string>());
                }

                headers[dotCount].Add(processedLine);
                fileCount++;
            }

            Debug.WriteLine("OneHeader: Found " + headers.Count + " nesting levels with total of " + fileCount + " headers");

            foreach(var entry in headers.Reverse())
            {
                List<string> levelHeaders = entry.Value;

                foreach(string header in levelHeaders)
                {
                    if(!File.Exists(header))
                    {
                        continue;
                    }

                    output += File.ReadAllText(header);
                }
            }
            */

            // Comment out #include directives
            //output = output.Replace("#include", "//#include");


            // Output collected single header
            //-------------------------------
            if (outputFile != null)
            {
                if (append)
                {
                    File.AppendAllText(outputFile, output);
                }
                else
                {
                    File.WriteAllText(outputFile, output);
                }
            }
            else
            {
                Console.WriteLine(output);
            }

            return true;
        }

        private bool CmdTextReplace(string options, out string result, List<string> content, int lineNumber)
        {
            result = "";

            string[] parts = options.Split(new char[] { ' ' });
            if (parts.Length < 3)
            {
                return false;
            }
            string inputFile = parts[0].Trim();
            string search = parts[1].Trim();
            string replace = parts[2].Trim();

            if (!File.Exists(inputFile))
            {
                Debug.WriteLine("TextReplace: InputFile does not exist '" + inputFile + "'");
                return false;
            }

            Debug.WriteLine("TextReplace: InputFile(" + inputFile + ") Search(" + search + ") Replace(" + replace + ")");

            string fileContent = File.ReadAllText(inputFile);
            fileContent = fileContent.Replace(search, replace);
            File.WriteAllText(inputFile, fileContent);

            return true;
        }

        private bool CmdWords(string options, out string result, List<string> content, int lineNumber)
        {
            string[] words = options.Split(' ');

            result = words.Length.ToString();

            return true;
        }

        private bool ShellCmdFind(string options, out string result, List<string> content, int lineNumber)
        {
            result = "";

            if (options == null || options.Length == 0)
            {
                return true;
            }

            options = Regex.Replace(options, @"\s+", " ");

            string[] optionParts = options.Split(' ');
            bool optionNameActive = false;
            bool optionTypeActive = false;
            List<string> filterList = new List<string>();
            string findPath = currentPath;
            bool filesOnly = false;
            bool foldersOnly = false;
            foreach(string option in optionParts)
            {
                switch(option)
                {
                    case "-name":
                        optionNameActive = true;
                        break;
                    case "-o":
                        // Ignore option 'or'
                        break;
                    case "-print":
                        // Ignore option '-print'
                        break;
                    case "-type":
                        optionTypeActive = true;
                        break;
                    default:
                        if(option.StartsWith("-"))
                        {
                            Console.Write("Unknown option '" + option + "' for shell command 'find'");
                            return false;
                        }
                        if(optionNameActive)
                        {
                            string filter = option.Trim().Replace("\"", "").Replace("'", "");
                            filterList.Add(filter);
                            optionNameActive = false;
                        }
                        else if(optionTypeActive)
                        {
                            if(option.Equals("d"))
                            {
                                foldersOnly = true;
                            }
                            else if(option.Equals("f"))
                            {
                                filesOnly = true;
                            }
                            optionTypeActive = false;
                        }
                        else
                        {
                            findPath = Path.Combine(currentPath, option);
                        }
                        break;
                }
            }

            if(!Directory.Exists(findPath))
            {
                Console.Write("Path does not exist for 'find' command: '" + findPath);
                return true;
            }

            // Walk through all files and folder in current path
            //--------------------------------------------------
            List<string> allElements = new List<string>();
            allElements.Add(findPath);
            if (filesOnly)
            {
                allElements.AddRange(Directory.GetFiles(findPath, "*", SearchOption.AllDirectories));
            }
            else
            {
                allElements.AddRange(Directory.GetFileSystemEntries(findPath, "*", SearchOption.AllDirectories));
            }
            foreach (string path in allElements)
            {
                string cleanPath = Utils.GetRelativePath(currentPath, path);

                // Check if file when file only filter is active
                //_---------------------------------------------
                if (filesOnly || foldersOnly)
                {
                    bool isFolder = false;
                    FileAttributes fileAttributes = File.GetAttributes(path);

                    isFolder = (fileAttributes & FileAttributes.Directory) == FileAttributes.Directory;
                    
                    if(isFolder && filesOnly)
                    {
                        continue;
                    }
                    if(!isFolder && foldersOnly)
                    {
                        continue;
                    }
                }

                if(filterList.Count == 0)
                {
                    result += cleanPath + " ";
                }

                foreach (string filter in filterList)
                {
                    if (filter.StartsWith("*"))
                    {
                        if (!cleanPath.EndsWith(filter.Substring(1)))
                        {
                            continue;
                        }
                    }
                    else if (filter.EndsWith("*"))
                    {
                        if (!cleanPath.StartsWith(filter.Substring(0, filter.Length - 1)))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (filter != cleanPath)
                        {
                            continue;
                        }
                    }

                    result += cleanPath + " ";
                    break;
                }
            }

            result = result.Trim();

            return true;
        }
    }
}
