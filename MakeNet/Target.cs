﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MakeNet.Parser
{
    public class Target
    {
        public string Name { get; }
        private List<string> content = new List<string>();
        private List<string> dependencies = new List<string>();

        public List<string> Content { get => content; }

        public List<string> Dependencies { get => dependencies; }

        public Target(string name, string dependenciesList)
        {
            Name = Utils.GetNormalizedPath(name.Trim());
            SetDependencies(dependenciesList);
        }
        public void SetDependencies(string dependenciesList)
        {
            string[] dependenciesParts = dependenciesList.Trim().Split(' ');
            foreach(string dependency in dependenciesParts)
            {
                if(dependency.Length == 0) continue;

                string fixedDependency = Utils.GetNormalizedPath(dependency.Trim());
                dependencies.Add(fixedDependency);
            }
        }
    }
}
