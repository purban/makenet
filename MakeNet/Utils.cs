﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace MakeNet
{
    public class Utils
    {
        // Holds application name as key and complete path as value
        private static Dictionary<string, string> pathApplicationsCache = new Dictionary<string, string>();

        // Holds all started and running processes by ExecuteCommand util function
        private static SynchronizedCollection<Process> startedProcesses = new SynchronizedCollection<Process>();

        public static void InitializePathApplicationCache()
        {
            string envTotalPath = Environment.GetEnvironmentVariable("PATH");
            string[] envPaths = envTotalPath.Split(new char[] { ';' });
            foreach(string envPath in envPaths)
            {
                if (!Directory.Exists("\\\\?\\" + envPath)) continue;

                string[] files = Directory.GetFiles("\\\\?\\" + envPath);
                foreach(string file in files)
                {
                    if (file.EndsWith(".bat") || file.EndsWith(".exe") || file.EndsWith(".vbs"))
                    {
                        string appName = Path.GetFileNameWithoutExtension(file);
                        if (!pathApplicationsCache.ContainsKey(appName))
                        {
                            string shortPath = GetShortPath(file).Replace("\\\\?\\", "");
                            //Debug.WriteLine("Add application to cache: Long(" + file + ") Short(" + shortPath + ")");
                            pathApplicationsCache.Add(appName, shortPath);
                        }
                    }
                }
            }
            Debug.WriteLine("Found " + pathApplicationsCache.Count + " applications in environment Paths");
        }

        public static bool ExecuteCmdProcess(string workingDir, string command, string arguments, bool directOutput, out List<string> output)
        {
            string directPath = null;

            // Normalize slashes
            //------------------
            command = command.Replace("/", "\\");

            // Check if command contains path to application
            //----------------------------------------------
            if(command.Contains("\\"))
            {
                directPath = command;
            }

            // Remove file extension from command
            //-----------------------------------
            if (command.EndsWith(".exe"))
            {
                command = command.Substring(0, command.Length - ".exe".Length);
            }

            // Check if cached application matches requested command
            //------------------------------------------------------
            if (directPath != null || pathApplicationsCache.TryGetValue(command, out directPath))
            {
                Debug.WriteLine("Execute Process: " + directPath + " " + arguments);
                return ExecuteProcess(workingDir, directPath, arguments, directOutput, out output);
            }
            // Else execute via CMD
            //---------------------
            else
            {
                string cmd = "/c \"" + command + " " + arguments + " 2>&1\"";
                Debug.WriteLine("Execute Process via CMD: cmd.exe " + cmd + " " + arguments);
                return ExecuteProcess(workingDir, "cmd.exe", cmd, directOutput, out output);
            }
        }

        public static bool ExecuteProcess(string workingDir, string command, string arguments, bool directOutput, out List<string> output)
        {
            const int RETRY_COUNT_MAX = 3;
            output = new List<string>();
            List<string> outputStream = new List<string>();
            List<string> errorStream = new List<string>();

            try
            {
                int retryCount = RETRY_COUNT_MAX;

                while (true)
                {
                    int exitCode = 0;
                    using (Process proc = new Process())
                    {
                        proc.StartInfo.FileName = command;
                        proc.StartInfo.Arguments = arguments;
                        proc.StartInfo.UseShellExecute = false;
                        proc.StartInfo.RedirectStandardOutput = true;
                        proc.StartInfo.RedirectStandardError = true;
                        proc.StartInfo.CreateNoWindow = true;
                        proc.StartInfo.WorkingDirectory = workingDir;

                        proc.OutputDataReceived += (sender, args) =>
                        {
                            if (string.IsNullOrEmpty(args.Data)) return;

                            if (directOutput)
                            {
                                Console.WriteLine(args.Data);
                            }
                            outputStream.Add(args.Data);
                        };

                        proc.ErrorDataReceived += (sender, args) =>
                        {
                            if (string.IsNullOrEmpty(args.Data)) return;

                            if (directOutput)
                            {
                                Console.WriteLine(args.Data);
                            }
                            errorStream.Add(args.Data);
                        };

                        startedProcesses.Add(proc);
                        proc.Start();
                        proc.BeginOutputReadLine();
                        proc.BeginErrorReadLine();
                        proc.WaitForExit();
                        startedProcesses.Remove(proc);
                        exitCode = proc.ExitCode;
                    }

                    if (exitCode != 0)
                    {
                        // If process failed try again multiple
                        // times to eliminate spontanious
                        // failues (seen with GCC)
                        // -> Only if no error was written
                        // -> Only if process is gcc
                        //-------------------------------------
                        retryCount--;

                        if ((command.ToLower().Contains("gcc") || command.ToLower().Contains("arm-li")) && 
                            errorStream.Count == 0 && outputStream.Count == 0 && retryCount > 0)
                        {
                            if (retryCount == RETRY_COUNT_MAX - 1)
                            {
                                output.Add("[Info] GCC error detected. Retry...");
                            }
                            outputStream.Clear();
                            errorStream.Clear();
                            continue;
                        }

                        // Copy collected output stream to ref
                        //------------------------------------
                        output.AddRange(outputStream);

                        // Write error to console
                        // if not yet written above
                        //-------------------------
                        if (!directOutput)
                        {
                            foreach (var line in errorStream)
                            {
                                Console.WriteLine(line);
                            }
                        }

                        output.Add("ExitCode(" + exitCode + ")"); // Add Exit code to result
                        return false;
                    }
                    else
                    {
                        if(!directOutput)
                        {
                            output.AddRange(outputStream);

                            // Also collect error stream if normal output is empty
                            if(outputStream.Count == 0 && errorStream.Count > 0)
                            {
                                output.AddRange(errorStream);
                            }
                        }
                    }

                    break;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public static void StopAllStartedProcesses()
        {
            List<Process> processList = new List<Process>(startedProcesses);
            startedProcesses.Clear();
            foreach (Process process in processList)
            {
                Debug.WriteLine("Kill process: " + process.ProcessName);
                process.Kill();
            }
        }

        public static string GetFormattedTimeSpan(TimeSpan timeSpan)
        {
            string formattedTimeSpan = "";

            if (timeSpan.Days != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Days + "d";
            }

            if (timeSpan.Hours != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Hours + "h";
            }

            if (timeSpan.Minutes != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Minutes + "m";
            }

            //if (timeSpan.Seconds != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Seconds + "." + timeSpan.Milliseconds + "s";
            }

            /*
            if (timeSpan.TotalSeconds == 0 && timeSpan.Milliseconds != 0)
            {
                if (formattedTimeSpan.Length != 0) formattedTimeSpan += " ";
                formattedTimeSpan += timeSpan.Milliseconds + "ms";
            }
            */

            return formattedTimeSpan;
        }

        public static string GetNormalizedPath(string rawPath)
        {
            // Check for Cygwin paths ('/mnt/c/...', '/c/...')
            //-----------------------
            if (rawPath.StartsWith("/") || rawPath.StartsWith("\\") || rawPath.StartsWith("mnt/") || rawPath.StartsWith("mnt\\"))
            {
                Regex regex = new Regex(@"^(([\\|\/]?mnt)?[\\|\/]?([a-zA-Z]+)[\\|\/]+)(.*)");
                Match match = regex.Match(rawPath);
                if (match.Success)
                {
                    string cygwinDrivePart = match.Groups[1].Value;
                    string driveLetter;
                    if (cygwinDrivePart.Contains("mnt"))
                    {
                        driveLetter = match.Groups[4].Value;
                    }
                    else
                    {
                        driveLetter = match.Groups[3].Value;
                    }

                    string remainingPath = rawPath.Substring(cygwinDrivePart.Length);
                    remainingPath = remainingPath.Replace("/", "\\");

                    rawPath = driveLetter + ":\\" + remainingPath;
                }
            }

            return rawPath;
        }

        public static string GetRelativePath(string currentPath, string totalPath)
        {
            if (IsSubDirectoryOf(totalPath, currentPath))
            {
                string relativePath = "";

                if(!totalPath.StartsWith(".\\") && !totalPath.StartsWith("./"))
                {
                    relativePath += ".\\";
                }

                relativePath += MakeRelativePath(currentPath, totalPath);

                return relativePath;
            }
            return totalPath;
        }

        public static bool IsSubDirectoryOf(string candidate, string other)
        {
            var isChild = false;
            try
            {
                var candidateInfo = new DirectoryInfo(candidate);
                var otherInfo = new DirectoryInfo(other);

                while (candidateInfo.Parent != null)
                {
                    if (candidateInfo.Parent.FullName == otherInfo.FullName)
                    {
                        isChild = true;
                        break;
                    }
                    else candidateInfo = candidateInfo.Parent;
                }
            }
            catch (Exception error)
            {
                var message = String.Format("Unable to check directories {0} and {1}: {2}", candidate, other, error);
                Console.WriteLine(message);
            }

            return isChild;
        }

        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path or <c>toPath</c> if the paths are not related.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

            if(!fromPath.EndsWith("\\"))
            {
                fromPath += "\\";
            }

            //Debug.WriteLine("Make relative path From(" + fromPath + ") To(" + toPath + ")");

            try
            {
                Uri fromUri = new Uri(fromPath);
                Uri toUri = new Uri(toPath);

                if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

                Uri relativeUri = fromUri.MakeRelativeUri(toUri);
                String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

                if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
                {
                    relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                }

                return relativePath;
            } catch(Exception)
            {
                return toPath;
            }
        }

        public static bool HasWriteAccessToFolder(string folderPath)
        {
            try
            {
                // Attempt to get a list of security permissions from the folder. 
                // This will raise an exception if the path is read only or do not have access to view the permissions. 
                System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
        }

        public static void AppendToEnvironmentVariable(string variableName, string append)
        {
            Debug.WriteLine("Append to environment variable '" + variableName + "': " + append);

            string envPath = Environment.GetEnvironmentVariable(variableName);
            string newPath = envPath + ";" + append;
            Environment.SetEnvironmentVariable(variableName, newPath);
        }

        const int MAX_PATH = 255;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetShortPathName(
            [MarshalAs(UnmanagedType.LPTStr)]
         string path,
            [MarshalAs(UnmanagedType.LPTStr)]
         StringBuilder shortPath,
            int shortPathLength
            );

        public static string GetShortPath(string path)
        {
            var shortPath = new StringBuilder(MAX_PATH);
            GetShortPathName(path, shortPath, MAX_PATH);
            return shortPath.ToString();
        }
    }
}
