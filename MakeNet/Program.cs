﻿using MakeNet.Parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MakeNet
{
    public class Program
    {
        private static string currentDirectory = Directory.GetCurrentDirectory();
        private static string makefileName = "Makefile";
        private static string makefilePath = null;
        private static string tempDirectory;
        private static bool versionRequested = false;

        private static void Cleanup()
        {
            try
            {
                Directory.Delete(tempDirectory, true);
            }
            catch (Exception) { };
        }
        private static void ExitApplication(string message, int error)
        {
            if (message != null)
            {
                Console.WriteLine(message);
            }

            Debug.WriteLine("Application will be exited with error code " + error);

//#if !DEBUG
            Environment.Exit(error);
//#endif

            Cleanup();
            return;
        }

        private static bool ExtractMkimage()
        {
            // Check if mkimage.exe is in same folder as this application
            //-----------------------------------------------------------
            string currentExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string[] files = Directory.GetFiles(currentExePath);
            foreach(string file in files)
            {
                if(file.EndsWith("mkimage.exe"))
                {
                    Debug.WriteLine("mkimage.exe already exists in same folder. Skip ressource extraction.");

                    // Found -> no ressource extraction required
                    //------------------------------------------
                    return true;
                }
            }

            Debug.WriteLine("Extract mkimage.exe to temp directory");

            // Create temp directory to unpack mkimage.exe
            //--------------------------------------------
            string tempSubDirectory = ".makenet";
            tempDirectory = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            if (Environment.OSVersion.Version.Major >= 6)
            {
                tempDirectory = Directory.GetParent(tempDirectory).ToString();
            }

            if (!Utils.HasWriteAccessToFolder(tempDirectory))
            {
                ExitApplication("No access to internal cache folder: " + tempDirectory, 1);
            }

            tempDirectory = Path.Combine(tempDirectory, tempSubDirectory);

            if (!Directory.Exists(tempDirectory))
            {
                Directory.CreateDirectory(tempDirectory);
            }

            // Add new temp directory to path
            //-------------------------------
            Utils.AppendToEnvironmentVariable("PATH", tempDirectory);

            try
            {
                string mkimagePath = Path.Combine(tempDirectory, "mkimage.exe");
                if(!File.Exists(mkimagePath))
                {
                    File.WriteAllBytes(mkimagePath, MakeNet.Properties.Resources.mkimage);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static bool ParameterProcessorVersionInfo(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;
            versionRequested = true;

            Assembly currentAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            string debug = "";
#if DEBUG
            debug = "[DEBUG]";
#endif
            Console.WriteLine("MakeNet v" + currentAssembly.GetName().Version + " " + debug);
            Console.WriteLine("GNU Make recode in .NET without additional dependencies.");
            Console.WriteLine("Nov 2021 by Philip Urban");
            return true;
        }

        private static bool ParameterProcessorPlainVersionInfo(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;
            versionRequested = true;

            Assembly currentAssembly = System.Reflection.Assembly.GetExecutingAssembly();

            string debug = "";
#if DEBUG
            debug = "[DEBUG]";
#endif
            Console.WriteLine("MakeNet v" + currentAssembly.GetName().Version + " " + debug);
            return true;
        }

        private static bool ParameterProcessorStrict(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            Parameters.Instance.FailIfUnknown = !Parameters.Instance.FailIfUnknown;

            return true;
        }

        private static bool ParameterProcessorDirecotry(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            // Check if option is set which helds directory path
            //--------------------------------------------------
            if (options == null || options.Count < 1)
            {
                Console.WriteLine("Argument '" + name + "' requires name of directory.");
                return false;
            }

            // Check if directory exists
            //--------------------------
            string directory = options[0];
            directory = Utils.GetNormalizedPath(directory);
            if(!Directory.Exists(directory))
            {
                Console.WriteLine("Given directory doens't exist '" + directory + "'");
                return false;
            }

            currentDirectory = Path.GetFullPath(directory);
            Debug.WriteLine("Set working directory to '" + currentDirectory + "'");
            

            optionsUsed = 1;

            return true;
        }

        private static bool ParameterProcessorFile(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            // Check if option is set which helds directory path
            //--------------------------------------------------
            if (options == null || options.Count < 1)
            {
                Console.WriteLine("Argument '" + name + "' requires name of file.");
                return false;
            }

            // Check if file exists
            //---------------------
            string file = options[0];
            if(!File.Exists(file))
            {
                Console.WriteLine("Given makefile doens't exist '" + file + "'");
                return false;
            }

            makefilePath = file;
            makefileName = Path.GetFileName(file);

            optionsUsed = 1;

            return true;
        }

        private static bool ParameterProcessorDebug(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            Debug.DebugMode = true;
            Debug.WriteLine("Debug mode enabled.");

            return true;
        }

        private static bool ParameterProcessorJobs(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            if(options.Count > 0 && int.TryParse(options[0], out BuildParameters.parallelJobCount))
            {
                optionsUsed = 1;
            }
            else
            {
                BuildParameters.parallelJobCount = (Environment.ProcessorCount*2) + 1;
            }

            Debug.WriteLine("Set maximum parallel tasks to " + BuildParameters.parallelJobCount);

            return true;
        }

        private static bool ParameterInternalCall(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            BuildParameters.internalCall = true;

            return true;
        }

        public static void Main(string[] args)
        {
            // Disable debug mode by default
            //------------------------------
#if DEBUG
            Debug.DebugMode = true;
#else
            Debug.DebugMode = false;
#endif

            Debug.WriteLine("Current direcotry is: " + currentDirectory);

            // Add current execution directory to path
            //----------------------------------------
            string currentExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Utils.AppendToEnvironmentVariable("PATH", currentExePath);

            // Extract mkimage.exe if required
            //--------------------------------
            if (!ExtractMkimage())
            {
                ExitApplication("Failed to extract mkimage", 5);
            }

            // Initialize application cache for command execution
            //---------------------------------------------------
            Utils.InitializePathApplicationCache();

            // Register Parameters
            //--------------------
            {
                Parameter versionParameter = new Parameter(ParameterProcessorVersionInfo, "Shows version information", true, "-v", "--version");
                Parameter plainVersionParameter = new Parameter(ParameterProcessorPlainVersionInfo, "Show plain version information", true, "-vp", "--version-plain");
                Parameter strictParameter = new Parameter(ParameterProcessorStrict, "Stop execution if any parameter is unknown", true, "-strict");
                Parameter directoryParameter = new Parameter(ParameterProcessorDirecotry, "Change to specific directory before reading the Makefile", true, "-C", "--directory");
                Parameter fileParameter = new Parameter(ParameterProcessorFile, "Execute specific Makefile", true, "-f", "--file");
                Parameter debugParameter = new Parameter(ParameterProcessorDebug, "Write debug information in file", true, "-d", "--debug");
                Parameter jobsParameter = new Parameter(ParameterProcessorJobs, "Set number of parallel executed jobs", true, "-j");
                Parameter InternalCallParameter = new Parameter(ParameterInternalCall, null, true, "-z"); // Internal command to Tag recursive make calls

                Parameters.Instance.RegisterParameter(versionParameter);
                Parameters.Instance.RegisterParameter(plainVersionParameter);
                Parameters.Instance.RegisterParameter(strictParameter);
                Parameters.Instance.RegisterParameter(directoryParameter);
                Parameters.Instance.RegisterParameter(fileParameter);
                Parameters.Instance.RegisterParameter(debugParameter);
                Parameters.Instance.RegisterParameter(jobsParameter);
                Parameters.Instance.RegisterParameter(InternalCallParameter);
            }

            // Parse given parameters
            //-----------------------
            List<string> remainingArguments;
            if (!Parameters.Instance.Parse(args, out remainingArguments))
            {
                // Error output handled in parameters class
                ExitApplication(null, 2);
            }

            // Search for Makefile in current path
            //------------------------------------
            if (makefilePath == null || makefilePath.Length == 0)
            {
                makefilePath = Path.Combine(currentDirectory, makefileName);
                if (!File.Exists(makefilePath))
                {
                    ExitApplication("Could not found Makefile '" + makefileName + "'", 3);
                }
            }
            Debug.WriteLine("Makefile Path: " + makefilePath);

            // Parse makefile to unwind Macros
            //--------------------------------
            Makefile makefile = new Makefile(makefilePath);
            Parser.Parser parser = new Parser.Parser(makefile, tempDirectory, currentDirectory);

            //parsedMakefile.SetVariable("MAKEFLAGS", string.Join(" ", args));

            // Extract variable declarations out of target list
            //-------------------------------------------------
            int listIndex = 0;
            while(listIndex < remainingArguments.Count)
            {
                string remainingArgument = remainingArguments[listIndex];
                listIndex++;

                if (remainingArgument.Contains("="))
                {
                    string[] parts = remainingArgument.Split(new char[] { '=' }, 2);
                    if(parts.Length >= 2)
                    {
                        makefile.SetVariableLocked(parts[0], parts[1]);
                    }
                    // Remove entry and start from beginning
                    //--------------------------------------
                    remainingArguments.Remove(remainingArgument);
                    listIndex = 0;
                }
            }

            if (!parser.Parse())
            {
                ExitApplication("Failed to parse Makefile", 4);
            }
            Debug.WriteLine("Finished parsing Makefile");

            // Execute targets
            //----------------
            Processor makefileExecuter = new Processor(parser, makefile);

            // Set default target if nothing selected
            //---------------------------------------
            if(remainingArguments.Count == 0 && !versionRequested)
            {
                Target defaultTarget = makefile.GetDefaultTarget();
                if(defaultTarget != null)
                {
                    Console.WriteLine("Execute default target: " + defaultTarget.Name);
                    remainingArguments.Add(defaultTarget.Name);
                }
            }
            Debug.WriteLine("Targets to execute: " + String.Join(", ", remainingArguments.ToArray()));

            if (!makefileExecuter.ExecuteTargets(remainingArguments))
            {
                // Error output handled in MakefileExecuter
                ExitApplication(null, 4);
            }

            // Write debug output
            //-------------------
            if (Debug.DebugMode)
            {
                string dumpPath = makefilePath + "_make_debug.log";
                Debug.WriteLine("Dump debug log to '" + dumpPath + "'");
                makefile.DumpContent(dumpPath);
            }

            // Exit with no error
            //-------------------
            ExitApplication(null, 0);
        }
    }
}
