﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MakeNet.Parser
{
    public class Macro
    {
        public string Name { get; }
        private List<string> content = new List<string>();

        public List<string> Content { get => content; }

        public Macro(string name)
        {
            Name = name;
        }
    }
}
