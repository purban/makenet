﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MakeNet.Parser
{
    public class Rules
    {
        private Makefile parsedMakefile;

        public Rules(Makefile parsedMakefile)
        {
            this.parsedMakefile = parsedMakefile;
        }

        public List<string> ClearInput(string[] rawInput)
        {
            List<string> clearedInput = new List<string>();
            foreach (string line in rawInput)
            {
                string lineToAdd = line;

                // Skip empty lines
                //-----------------
                if (line.Trim().Length == 0)
                {
                    continue;
                }

                // Skip comment lines
                //-------------------
                if (!SkipCommentLine(lineToAdd, out lineToAdd)) return null;

                // Compact multilines
                //-------------------
                if (!CompactMultilines(lineToAdd, out lineToAdd)) return null;

                // Replace diamond brackets
                //-------------------------
                if (!NormalizeBrackets(lineToAdd, out lineToAdd)) return null;

                if (lineToAdd != null)
                {
                    clearedInput.Add(lineToAdd);
                }
            }

            return clearedInput;
        }

        public bool LineIsAnyVariableDeclaration(string line, out Match match)
        {
            Regex regex = new Regex(@"^[\s]*([a-zA-Z_]+[a-zA-Z_\d]*)[\s]*([+-=:]?[=]){1}(.*)");
            match = regex.Match(line);

            return match.Success;
        }

        public bool LineIsExecutingVariableDeclaration(string line, out Match match)
        {
            Regex regex = new Regex(@"^[\s]*([a-zA-Z_]+[a-zA-Z_\d]*)[\s]*(:[=]){1}(.*)");
            match = regex.Match(line);

            return match.Success;
        }

        public bool LineIsVariableDeclaration(string line)
        {
            Match match;
            return LineIsAnyVariableDeclaration(line, out match);
        }

        public bool LineIsExecutingVariableDeclaration(string line)
        {
            Match match;
            return LineIsExecutingVariableDeclaration(line, out match);
        }

        public bool StoreLineVariable(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;
            bool export = false;

            // Handle export variables
            //------------------------
            if(line.Trim().StartsWith("export"))
            {
                export = true;
                line = line.Substring("export".Length);
            }

            Match match;
            if(LineIsAnyVariableDeclaration(line, out match))
            {
                string variableName = match.Groups[1].Value;
                string variableOperator = match.Groups[2].Value;
                string variableValue = match.Groups[3].Value;
                string existingVariable;
                bool force = false;
                existingVariable = parsedMakefile.GetVariable(variableName);
                if (existingVariable == null)
                {
                    existingVariable = "";
                }

                // Process operator
                //-----------------
                switch (variableOperator)
                {
                    case "=":
                        existingVariable = variableValue;
                        break;
                    case ":=":
                        existingVariable = variableValue;
                        break;
                    case "+=":
                        existingVariable = existingVariable + variableValue;
                        force = true;
                        break;
                    case "-=":
                        // Not supported
                        //--------------
                        Console.WriteLine("Variable operator '-=' not supported.");
                        force = true;
                        return false;
                    case "/=":
                        // Not supported
                        //--------------
                        Console.WriteLine("Variable operator '/=' not supported.");
                        force = true;
                        return false;
                    case "*=":
                        // Not supported
                        //--------------
                        Console.WriteLine("Variable operator '*=' not supported.");
                        force = true;
                        return false;
                }

                // Add variable
                //-------------
                if(force)
                {
                    parsedMakefile.SetVariableIgnoreLock(variableName, existingVariable);
                }
                else
                {
                    parsedMakefile.SetVariable(variableName, existingVariable);
                }
                if (export)
                {
                    parsedMakefile.SetVariableToExport(variableName);
                }
                return true;
            }
            lineToStore = line;
            return true;
        }

        public bool AddPhonies(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            if(line.TrimStart().StartsWith(".PHONY:"))
            {
                string phonyList = line.TrimStart().Substring(".PHONY:".Length).Trim();
                string[] phonies = phonyList.Split(' ');
                parsedMakefile.AddPhonies(phonies);
            }
            else
            {
                lineToStore = line;
            }

            return true;
        }

        public bool ReplaceLineVariables(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            // Search for 'normal' variables like $(variable)
            //-----------------------------------------------
            Regex regex = new Regex(@"\$(([\(|\{]{1}[a-zA-Z_]+[a-zA-Z_\d]*[\)|\}]{1})|([a-zA-Z_]+[a-zA-Z_\d]*)|([\(|\{]{1}[a-zA-Z_]+[a-zA-Z_\d]*\:[a-zA-Z_\d\.\=]+[\)|\}]{1}))");
            var matches = regex.Matches(line).OfType<Match>();
            
            // Only variables which are NOT part of a ForeEach-Text must be replaced here!
            // Hint: This check must be done outside of the foreach-loop because the foreach-loop will change the line and check the location of the match will fail
            var lineText = line;
            matches = matches.Where(m => !IsForeachVariable(m, lineText));
            foreach (Match match in matches)
            {
                string wholeExpression = match.Groups[0].Value;
                string variableName = match.Groups[1].Value;
                variableName = variableName.Replace("(", "").Replace(")", "");
                variableName = variableName.Replace("{", "").Replace("}", "");
                string[] variableParts = variableName.Split(new char[] { ':' }, 2);
                string variableOptions = null;
                if(variableParts.Length == 2)
                {
                    variableOptions = variableParts[1];
                    variableName = variableParts[0];
                }
                
                string variableValue;
                variableValue = parsedMakefile.GetVariable(variableName);
                if (variableValue == null)
                {
                    variableValue = "";
                }

                // Handle replacement option 
                // $(VARAIBLE:from=to)
                if(variableOptions != null)
                {
                    string[] optionParts = variableOptions.Split(new char[] { '=' }, 2);
                    if(optionParts.Length == 2)
                    {
                        string from = optionParts[0];
                        string to = optionParts[1];

                        variableValue = variableValue.Replace(from, to);
                    }
                }

                line = line.Replace(wholeExpression, variableValue);
            }

            lineToStore = line;
            return true;
        }

        private static bool IsForeachVariable(Match match, string line)
        {
            // Check if text part of foreach
            // $(foreach var,list,text)
            // Each variable inside 'text' which equals 'var' must not be replaced
            //--------------------------------------------------------------------
            string variableName = match.Groups[1].Value;
            variableName = variableName.Replace("(", "").Replace(")", "");
            variableName = variableName.Replace("{", "").Replace("}", "");

            bool isForeachVariable = false;
            int foreachIndex = -1;
            do
            {
                foreachIndex = line.Substring(foreachIndex + 1).IndexOf("$(foreach ");
                if (foreachIndex > -1 && match.Index > foreachIndex)
                {
                    int lineIndex = 0;
                    int openBrackets = 0;
                    int currentActiveOption = 0; // 0 == var, 1 == list, 2 == text
                    int currentStartIndex = foreachIndex + "$(foreach ".Length;
                    string var = null;
                    string list = null;
                    string text = null;
                    for (lineIndex = currentStartIndex;
                        lineIndex < line.Length;
                        lineIndex++)
                    {
                        if (line[lineIndex] == ',' && openBrackets == 0)
                        {
                            currentActiveOption++;

                            switch (currentActiveOption)
                            {
                                // var
                                case 1:
                                    var = line.Substring(currentStartIndex, lineIndex - currentStartIndex);
                                    currentStartIndex = lineIndex;
                                    break;
                                // list
                                case 2:
                                    list = line.Substring(currentStartIndex + 1, lineIndex - currentStartIndex - 1);
                                    list = list.Trim();
                                    currentStartIndex = lineIndex;
                                    break;
                            }
                        }

                        if (line[lineIndex] == '(')
                        {
                            openBrackets++;
                        }
                        else if (line[lineIndex] == ')')
                        {
                            if (currentActiveOption == 2 && openBrackets == 0)
                            {
                                break;
                            }

                            openBrackets--;
                        }
                    }

                    int textStartIndex = currentStartIndex + 1;
                    int textEndIndex = lineIndex;
                    text = line.Substring(textStartIndex, textEndIndex - currentStartIndex);
                    text = text.Trim();

                    // All variables inside text which equals var must not be replaced

                    // Check if match is inside text
                    //------------------------------
                    if (match.Index >= textStartIndex && match.Index + match.Length + ")".Length <= textEndIndex)
                    {
                        if (var == variableName)
                        {
                            // Skip foreach 'var' variable
                            //----------------------------
                            isForeachVariable = true;
                        }
                    }
                }
            } while (foreachIndex != -1);

            return isForeachVariable;
        }

        public bool SkipCommentLine(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            int numSignIndex = line.IndexOf("#");

            // There is no #-Character or the character itself is not escaped '\#'
            if (numSignIndex == -1 || (numSignIndex > 0 && line[numSignIndex - 1] == '\\'))
            {
                lineToStore = line;
            }
            else
            {
                lineToStore = line.Substring(0, line.Length - (line.Length - numSignIndex));
                if (lineToStore.Trim().Length == 0)
                {
                    lineToStore = null;
                }
            }

            return true;
        }

        public bool NormalizeBrackets(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            lineToStore = line.Replace("{", "(").Replace("}", ")");

            return true;
        }

        private string multiLine = null;
        public bool CompactMultilines(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            if (line.EndsWith(@"\"))
            {
                line = line.Replace('\\', ' ');
                line = line.TrimEnd();
                if (multiLine == null)
                {
                    multiLine = line;
                }
                else
                {
                    multiLine += " " + line.TrimStart();
                }
                lineToStore = null;
            }
            else if (multiLine != null)
            {
                multiLine += " " + line.Trim();
                lineToStore = multiLine;
                multiLine = null;
            }
            else
            {
                lineToStore = line;
            }

            return true;
        }

        private void DebugIfClause(string message)
        {
            //Debug.WriteLine(message);
        }

        class IfClauseState
        {
            public bool ElseBranchActive { get; set; }
            public bool CheckIfBranch { get; set; }
            public bool SkipNextLine { get; set; }

            public IfClauseState()
            {
                ElseBranchActive = false;
                CheckIfBranch = true;
                SkipNextLine = false;
            }
        }
        List<IfClauseState> ifClauseStates = new List<IfClauseState>();
        int currentIfClauseLevel = -1;
        int currentInactiveIfClauseLevel = -1;
        public bool CheckIfClause(string line, out string lineToStore)
        {
            bool ifClauseLine = false;

            lineToStore = null;
            if (string.IsNullOrEmpty(line)) return true;

            // endif
            //------
            if (line.TrimStart().StartsWith("endif"))
            {
                if(currentInactiveIfClauseLevel > -1)
                {
                    DebugIfClause("Found inactive 'endif'");
                    currentInactiveIfClauseLevel--;
                    DebugIfClause("inactive-if-clause-level-- = " + currentInactiveIfClauseLevel);
                }
                else
                {
                    DebugIfClause("Found 'endif'");

                    ifClauseLine = true;

                    if (currentIfClauseLevel == -1)
                    {
                        Console.WriteLine("'endif' without beginning of if-clause.");
                        lineToStore = null;
                        return false;
                    }

                    ifClauseStates.RemoveAt(currentIfClauseLevel);
                    currentIfClauseLevel--;
                    DebugIfClause("if-clause-level-- = " + currentIfClauseLevel);
                }
            }

            // else
            //-----
            if (line.TrimStart().StartsWith("else"))
            {
                DebugIfClause("Found 'else'");

                if (currentIfClauseLevel < 0)
                {
                    Console.WriteLine("'else' without beginning of if-clause.");
                    return false;
                }

                ifClauseLine = true;
                line = line.Replace("else", "");
                line = line.Trim();

                if(!ifClauseStates[currentIfClauseLevel].SkipNextLine && !ifClauseStates[currentIfClauseLevel].ElseBranchActive)
                {
                    ifClauseStates[currentIfClauseLevel].SkipNextLine = true;
                    ifClauseStates[currentIfClauseLevel].CheckIfBranch = false;
                }

                else if(ifClauseStates[currentIfClauseLevel].SkipNextLine && line.Length == 0 && !ifClauseStates[currentIfClauseLevel].ElseBranchActive)
                {
                    ifClauseStates[currentIfClauseLevel].SkipNextLine = false;
                }

                // Last 'else' in if-else-if-else tree
                else if (!ifClauseStates[currentIfClauseLevel].SkipNextLine && line.Length == 0 && ifClauseStates[currentIfClauseLevel].ElseBranchActive)
                {
                    ifClauseStates[currentIfClauseLevel].SkipNextLine = true;
                }

                ifClauseStates[currentIfClauseLevel].ElseBranchActive = true;
            }

            // Check ifeq-ifneq
            //-----------------
            Regex regex = new Regex(@"if([neq]+)\s*\(([a-zA-Z_\d\s\$\(\),\/*.\-\'\{\}:\\=]*)\)");
            Match match = regex.Match(line);
            if (match.Success)
            {
                DebugIfClause("Found 'if'");

                if (currentIfClauseLevel == -1 || !ifClauseStates[currentIfClauseLevel].ElseBranchActive || 
                    (ifClauseStates[currentIfClauseLevel].ElseBranchActive && !ifClauseLine))
                {
                    currentIfClauseLevel++;
                    DebugIfClause("if-clause-level++ = " + currentIfClauseLevel);
                    ifClauseStates.Add(new IfClauseState());
                }
                else if(!ifClauseLine)
                {
                    currentInactiveIfClauseLevel++;
                    DebugIfClause("inactive-if-clause-level++ = " + currentInactiveIfClauseLevel);
                }
                ifClauseLine = true;

                if (ifClauseStates[currentIfClauseLevel].CheckIfBranch)
                {
                    string ifClauseType = match.Groups[1].Value;
                    string argument = match.Groups[2].Value;

                    string[] argumentParts = argument.Split(new char[] { ',' }, 2);
                    if (argumentParts.Length < 2)
                    {
                        Console.WriteLine("Argument count of " + ifClauseType + " is less than 2.");
                        lineToStore = null;
                        return false;
                    }

                    string arg1 = argumentParts[0];
                    string arg2 = argumentParts[1];

                    if (ifClauseType == "eq")
                    {
                        ifClauseStates[currentIfClauseLevel].SkipNextLine = arg1.Trim() != arg2.Trim();
                    }
                    else if (ifClauseType == "neq")
                    {
                        ifClauseStates[currentIfClauseLevel].SkipNextLine = arg1.Trim() == arg2.Trim();
                    }
                    else
                    {
                        Console.WriteLine("Unsupported type of if-clause '" + ifClauseType + "'.");
                        lineToStore = null;
                        return false;
                    }
                }
            }

            // Check if line must be skipped
            //------------------------------
            if ((currentIfClauseLevel > -1 && ifClauseStates[currentIfClauseLevel].SkipNextLine) || ifClauseLine)
            {
                lineToStore = null;
            }
            else
            {
                lineToStore = line;
            }

            return true;
        }

        public bool CheckIfClauseSkipped(string line, out string lineToStore)
        {
            lineToStore = line;
            if(line == null) return true;

            // Check if no if clause is active
            //--------------------------------
            if(currentIfClauseLevel < 0)
            {
                return true;
            }

            // Check if if clause line itself
            //-------------------------------
            if(line.TrimStart().StartsWith("ifeq") || line.TrimStart().StartsWith("ifneq") ||
                line.TrimStart().StartsWith("else") || line.TrimStart().StartsWith("endif"))
            {
                return true;
            }

            for(int level = 0; level <= currentIfClauseLevel; level++)
            {
                if(ifClauseStates[level].SkipNextLine)
                {
                    lineToStore = null;
                }
            }

            return true;
        }

        private Macro currentMacro = null;
        public bool ExtractMacros(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            // Save macro content
            //-------------------
            if (currentMacro != null)
            {
                // Check for end of macro
                //-----------------------
                if (line.StartsWith("endef"))
                {
                    currentMacro = null;
                    return true;
                }
                else
                {
                    currentMacro.Content.Add(line);
                    return true;
                }
            }

            // Check for macro start
            //----------------------
            Regex regex = new Regex(@"^define\s([a-zA-Z_\d]+)[\s]*");
            Match match = regex.Match(line);
            if (match.Success)
            {
                string macroName = match.Groups[1].Value;

                currentMacro = new Macro(macroName);
                parsedMakefile.AddMacro(currentMacro);
                return true;
            }

            lineToStore = line;
            return true;
        }

        private Target currentTarget = null;
        public bool ExtractTargets(string line, out string lineToStore)
        {
            lineToStore = null;
            if (line == null) return true;

            // Save target content
            //--------------------
            if(currentTarget != null)
            {
                // Check for end of target
                //------------------------
                if (!line.StartsWith(" ") && !line.StartsWith("\t"))
                {
                    currentTarget = null;
                }
                else
                {
                    currentTarget.Content.Add(line.Trim());
                    return true;
                }
            }

            // Check for empty target definitions 
            // (e.g. due to calculated target name)
            //-------------------------------------
            if (line.Trim() == ":")
            {
                string targetName = "__empty_target__";
                string dependencies = "";

                currentTarget = new Target(targetName, dependencies);
                return true;
            }

            // Check for target start
            //-----------------------
            Regex regex = new Regex(@"^([a-zA-Z\/\%\-\.\\\d_-]+)[\s]*\:[\s.+]?(.*)");
            Match match = regex.Match(line);
            if (match.Success)
            {
                string targetName = match.Groups[1].Value;
                string dependencies = match.Groups[2].Value;

                currentTarget = new Target(targetName, dependencies);
                parsedMakefile.AddTarget(currentTarget);
                return true;
            }

            lineToStore = line;
            return true;
        }

        public bool ProcessIncludes(string line, out string lineToStore, List<string> content, int lineNumber, string currentPath)
        {
            lineToStore = null;
            if (line == null) return true;

            if (line.TrimStart().StartsWith("include"))
            {
                string[] lines;
                string includeArg = line.TrimStart().Substring("include".Length).Trim();
                string[] includePaths = includeArg.Split(" ".ToCharArray());
                bool first = true;
                foreach(string includePath in includePaths)
                {
                    string includePathTrimmed = includePath.Trim();
                    if (includePathTrimmed.Length == 0) continue;

                    includePathTrimmed = Utils.GetNormalizedPath(includePathTrimmed);
                    includePathTrimmed = Path.Combine(currentPath, includePathTrimmed);

                    // Read content of makefile
                    //-------------------------
                    try
                    {
                        lines = File.ReadAllLines(includePathTrimmed);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to read included file into: " + e.Message);
                        lineToStore = null;
                        return false;
                    }

                    if(first)
                    {
                        // Remove line with incldue statement
                        //-----------------------------------
                        content.RemoveAt(lineNumber);

                        // Add dummy line which will be removed by caller
                        // to match normal rule behavior
                        //-----------------------------------------------
                        content.Insert(lineNumber, "DUMMY");
                        lineNumber += 1;

                        first = false;
                    }

                    // Clear new input file (comments etc.)
                    //-------------------------------------
                    List<string> includedInput = ClearInput(lines);

                    // Copy new lines into existing content
                    //-------------------------------------
                    content.InsertRange(lineNumber, includedInput);
                    lineNumber += includedInput.Count;
                }

                if(!first)
                {
                    lineToStore = null;
                    return true;
                }
            }

            lineToStore = line;
            return true;
        }
    }
}
