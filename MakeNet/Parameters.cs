﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace MakeNet
{    
    class Parameter
    {
        public delegate bool ProcessorType(string name, List<string> options, out int optionsUsed);
        List<string> targets = new List<string>();

        public Parameter(ProcessorType processor, string infoText, bool executeOnce, string name, params string[] aliases)
        {
            Aliases = new List<string>();
            Aliases.Add(name);
            Aliases.AddRange(aliases);
            Processor = processor;
            InfoText = infoText;
            ExecuteOnce = executeOnce;
            Executed = false;
            ShownInHelp = false;
        }

        public List<string> Aliases { get; set; }

        public string InfoText { get; set; }

        public ProcessorType Processor { get; set; }

        public bool ExecuteOnce { get; set; }

        public bool Executed { get; set; }

        public bool ShownInHelp { get; set; }
    }

    class Parameters
    {
        private static Parameters instance = null;
        private static readonly object padlock = new object();

        private Dictionary<string, Parameter> parameters = new Dictionary<string, Parameter>();

        private Parameters()
        {
            Parameter helpParameter = new Parameter(
                ParameterHelpProcessor, 
                "Shows available parameters", 
                true, 
                "-?", "-help", "--help");
            RegisterParameter(helpParameter);
        }

        public static Parameters Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Parameters();
                    }
                    return instance;
                }
            }
        }

        public bool FailIfUnknown { get; set; }

        public bool Parse(string[] args, out List<string> unknownParameters)
        {
            unknownParameters = new List<string>();
            bool splittedOption = false;

            // Loop through arguments
            //-----------------------
            for (int argIndex = 0; argIndex < args.Length; argIndex++)
            {
                string arg = args[argIndex];
                List<string> options = new List<string>();
                Parameter parameter = null;
                bool isParameter = arg.StartsWith("-");

                // Check if parameter instead of option
                //-------------------------------------
                if (isParameter)
                {
                    // Split option from parameter if directly attached (only numbers)
                    //----------------------------------------------------------------
                    Regex regex = new Regex(@"(^-[a-zA-Z_]+)([\d]+)");
                    Match match = regex.Match(arg);
                    if (match.Success)
                    {
                        arg = match.Groups[1].Value;
                        options.Add(match.Groups[2].Value);
                        splittedOption = true;
                    }

                    parameters.TryGetValue(arg, out parameter);
                }

                // Collect options
                //----------------
                for (int optionIndex = argIndex + 1; optionIndex < args.Length; optionIndex++)
                {
                    string option = args[optionIndex];
                    bool isOption = !option.StartsWith("-");

                    // Check if next argument is not an option
                    //----------------------------------------
                    if (!isOption) break;

                    // Add to options list
                    //--------------------
                    options.Add(option);
                }

                // Execute processor if available
                //-------------------------------
                if(parameter != null)
                {
                    if(!parameter.Executed || !parameter.ExecuteOnce)
                    {
                        int optionsUsed;
                        if (!parameter.Processor(arg, options, out optionsUsed))
                        {
                            // Failed to process parameter
                            //----------------------------
                            return false;
                        }

                        // Skip used options
                        //------------------
                        if(splittedOption && optionsUsed > 0)
                        {
                            splittedOption = false;
                            optionsUsed--;
                        }

                        argIndex += optionsUsed;

                        // Mark parameter as executed
                        //---------------------------
                        parameter.Executed = true;
                    }
                }
                else if(isParameter && FailIfUnknown)
                {
                    // Unknown parameter
                    //------------------
                    Console.WriteLine("Unknown Parameter '" + arg + "'. Stop.");
                    return false;
                }
                else if(!isParameter)
                {
                    unknownParameters.Add(arg);
                }
            }

            // OK processed all parameters
            //----------------------------
            return true;
        }

        public bool RegisterParameter(Parameter parameter)
        {
            foreach (string alias in parameter.Aliases)
            {
                parameters.Add(alias, parameter);
            }

            return true;
        }

        private bool ParameterHelpProcessor(string name, List<string> options, out int optionsUsed)
        {
            optionsUsed = 0;

            // Check if version parameter is registered
            // and execute this before
            //-----------------------------------------
            if (parameters.ContainsKey("-v"))
            {
                Parameter versionParameter;
                if(parameters.TryGetValue("-v", out versionParameter))
                {
                    if (!versionParameter.Executed)
                    {
                        versionParameter.Executed = true;
                        versionParameter.Processor("-v", new List<string>(), out optionsUsed);
                    }
                    Console.WriteLine("");
                }
            }

            foreach (KeyValuePair<string, Parameter> entry in parameters)
            {
                int aliasTextCharWidth = 50;
                Parameter parameter = entry.Value;

                // Check if already shown
                //-----------------------
                if (parameter.ShownInHelp) continue;
                parameter.ShownInHelp = true;

                // Check if must be hidden in help (text is null)
                //-----------------------------------------------
                if (parameter.InfoText == null) continue;

                // Build readable comma separated alias list
                //------------------------------------------
                string parameterAliases = null;
                foreach(string alias in parameter.Aliases)
                {
                    if(parameterAliases != null)
                    {
                        parameterAliases += ", ";
                    }
                    parameterAliases += alias;
                }

                string helpLine = parameterAliases;
                for(int remainingChars = parameterAliases.Length; remainingChars < aliasTextCharWidth; remainingChars++)
                {
                    helpLine += " ";
                }
                helpLine += ": " + parameter.InfoText;

                Console.WriteLine(helpLine);
            }

            return true;
        }
    }
}
