﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeNet
{
    static class Debug
    {
        private static DateTime start = DateTime.Now;

        public static bool DebugMode { get; set; }

        public static void WriteLine(string text)
        {
            if(DebugMode)
            {
                TimeSpan timeElapsed = DateTime.Now.Subtract(start);
                string timeElapsedFormatted = String.Format("{0:0.000}", timeElapsed.TotalSeconds);
                Console.WriteLine("[Debug|" + timeElapsedFormatted + "] " + text);
            }
        }
    }
}
