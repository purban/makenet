﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MakeNet.Parser
{
    public class Makefile
    {
        private List<string> phonies = new List<string>();
        private Dictionary<string, string> variables = new Dictionary<string, string>();
        private Dictionary<string, string> variablesLocked = new Dictionary<string, string>();
        private List<string> exported = new List<string>();
        private List<string> makefileContent = new List<string>();
        private List<Target> targets = new List<Target>();
        private Dictionary<string, Macro> macros = new Dictionary<string, Macro>();

        public string Path { get; }

        public List<string> TextContent { get => makefileContent; }

        public List<string> ExportedVariables { get => exported; }

        public Makefile(string path)
        {
            Path = path;

            // Add default variabme $(MAKE)
            //-----------------------------
            SetVariable("MAKE", "__MAKE__");

            // Add internal call default variable
            //-----------------------------------
            if(BuildParameters.internalCall)
            {
                SetVariable("_MAKE_RECALL_", "TRUE");
            }
            else
            {
                SetVariable("_MAKE_NO_RECALL_", "TRUE");
            }
        }

        public void AddPhonies(string[] phonies)
        {
            this.phonies.AddRange(phonies);
        }

        public bool IsPhony(string name)
        {
            return phonies.Contains(name);
        }

        public void AddTarget(Target target)
        {
            targets.Add(target);
        }

        public Target GetDefaultTarget()
        {
            foreach(Target target in targets)
            {
                if(!target.Name.StartsWith("."))
                {
                    return target;
                }
            }
            return null;
        }

        public Target GetTarget(Parser makefileParser, string targetName)
        {
            List<Target> possibleTargets = new List<Target>();

            foreach(Target target in targets)
            {
                // Check for exact match
                //----------------------
                if(target.Name.Equals(targetName))
                {
                    return target;
                }

                // Check for wildcard match
                //-------------------------
                if(target.Name.StartsWith("%"))
                {
                    if(targetName.EndsWith(target.Name.Substring(1)))
                    {
                        possibleTargets.Add(target);
                    }
                }
            }

            if(possibleTargets.Count == 1)
            {
                return possibleTargets[0];
            }

            Target foundTarget = null;
            foreach(Target target in possibleTargets)
            {
                foundTarget = target;

                if (target.Dependencies.Count == 0 || target.Dependencies.Count > 1) continue;

                string dependency = target.Dependencies[0];
                string targetPath = targetName;
                if(dependency.StartsWith("%"))
                {
                    dependency = dependency.Substring(1);
                }
                if(target.Name.StartsWith("%"))
                {
                    targetPath = targetPath.Substring(0, targetPath.Length - (target.Name.Length - 1));
                }

                string dependencyFilePath = System.IO.Path.Combine(makefileParser.CurrentPath, targetPath + dependency);

                if (File.Exists(dependencyFilePath))
                {
                    Debug.WriteLine("Found explicit matching target for '" + dependencyFilePath + "': '" + target.Name + ": " + target.Dependencies[0] + "'");
                    foundTarget = target;
                    break;
                }
            }

            return foundTarget;
        }

        public void AddMacro(Macro macro)
        {
            macros.Add(macro.Name, macro);
        }

        public Dictionary<string, string> GetAllVariables()
        {
            return variables;
        }

        public string GetVariable(string name)
        {
            string variableValue = null;
            if(!variables.TryGetValue(name, out variableValue))
            {
                switch(name)
                {
                    case "__TIME__":
                        variableValue = DateTime.Now.ToString("hh:mm:ss:fff");
                        break;
                    case "__DATE__":
                        variableValue = DateTime.Now.ToString("dd.MM.yyyy");
                        break;
                }
            }
            return variableValue;
        }

        public bool isAlwaysRenew(string filePath)
        {
            string alwaysRenewList = GetVariable("_MAKE_ALWAYS_RENEW");
            if(alwaysRenewList == null)
            {
                return false;
            }

            string[] entries = alwaysRenewList.Split(' ');
            foreach(string entry in entries)
            {
                if(filePath.Contains(entry))
                {
                    Debug.WriteLine("Found entry in '_MAKE_ALWAYS_RENEW' list: " + filePath);
                    return true;
                }
            }

            return false;
        }

        public Macro GetMacro(string name)
        {
            Macro macro;
            macros.TryGetValue(name, out macro);
            return macro;
        }

        public void SetVariable(string name, string value)
        {
            if (variablesLocked.ContainsKey(name))
            {
                return;
            }

            variables.Remove(name);
            variables.Add(name, value.Trim());
        }

        public void SetVariableLocked(string name, string value)
        {
            if(variablesLocked.ContainsKey(name))
            {
                return;
            }

            SetVariable(name, value);
            variablesLocked.Add(name, value.Trim());
        }

        public void SetVariableIgnoreLock(string name, string value)
        {
            variables.Remove(name);
            variables.Add(name, value.Trim());
        }

        public void SetVariableToExport(string name)
        {
            if(!exported.Contains(name))
            {
                exported.Add(name);
            }
        }

        public void DumpContent(string filePath)
        {
            List<string> dumpContent = new List<string>();

            // Add makefile text content
            //--------------------------
            if (makefileContent.Count > 0)
            {
                dumpContent.Add("--- Not parsable content ---");
                foreach (string lines in makefileContent)
                {
                    dumpContent.Add(lines);
                }
                dumpContent.Add("");
            }

            // Add phonies
            //------------
            dumpContent.Add("--- Phonies ---");
            int phonyNum = 0;
            foreach (string phony in phonies)
            {
                phonyNum++;
                dumpContent.Add(phonyNum + ": " + phony);
            }
            dumpContent.Add("");

            // Add targets
            //------------
            dumpContent.Add("--- Targets ---");
            int targetNum = 0;
            foreach (Target target in targets)
            {
                targetNum++;
                dumpContent.Add(targetNum + ": " + target.Name);

                if(target.Dependencies.Count > 0)
                {
                    dumpContent.Add("\tDependencies:");
                    foreach(string dependency in target.Dependencies)
                    {
                        dumpContent.Add("\t\t" + dependency);
                    }
                }
                if(target.Content.Count > 0)
                {
                    dumpContent.Add("\tContent:");
                    foreach(string content in target.Content)
                    {
                        dumpContent.Add("\t\t" + content);
                    }
                }
            }
            dumpContent.Add("");

            // Add macros
            //-----------
            dumpContent.Add("--- Macros ---");
            int macroNum = 0;
            foreach (KeyValuePair<string, Macro> entry in macros)
            {
                macroNum++;
                Macro macro = entry.Value;
                dumpContent.Add(macroNum + ": " + entry.Key);
            }
            dumpContent.Add("");

            // Add variable list
            //------------------
            dumpContent.Add("--- Variables ---");
            int variableNum = 0;
            foreach (KeyValuePair<string, string> entry in variables)
            {
                variableNum++;
                string[] valueParts = entry.Value.Split(' ');
                dumpContent.Add(variableNum + ": " + entry.Key);
                foreach(string value in valueParts)
                {
                    dumpContent.Add("\t" + value);
                }
            }
            dumpContent.Add("");

            // Save dump in file
            //------------------
            File.WriteAllLines(filePath, dumpContent.ToArray());
        }
    }
}
