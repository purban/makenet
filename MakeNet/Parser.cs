﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace MakeNet.Parser
{
    public class Parser
    {
        public Rules Rules { get; }
        public Commands Commands { get; }
        public string CurrentPath { get; }
        Makefile parsedMakefile;

        public Parser(Makefile parsedMakefile, string tempDirectoryPath, string currentPath)
        {
            this.parsedMakefile = parsedMakefile;
            CurrentPath = currentPath;
            Rules = new Rules(parsedMakefile);
            Commands = new Commands(CurrentPath, tempDirectoryPath, parsedMakefile, Rules);
        }

        public bool Parse()
        {
            string[] lines;

            // Read content of makefile
            //-------------------------
            try
            {
                lines = File.ReadAllLines(parsedMakefile.Path);
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to read file: " + e.Message);
                return false;
            }

            return ParseContent(lines); 
        }
    
        private bool ParseContent(string[] makefileContent)
        {
            // Remove comments, empty lines and compact multilines
            //----------------------------------------------------
            parsedMakefile.TextContent.AddRange(Rules.ClearInput(makefileContent));

            // Process default text based topics
            //----------------------------------
            for (int lineNumber = 0; lineNumber < parsedMakefile.TextContent.Count; lineNumber++)
            {
                string line = parsedMakefile.TextContent[lineNumber];
                string lineToAdd = line;

                // Check if skipped by if-clause
                //------------------------------
                if (!Rules.CheckIfClauseSkipped(lineToAdd, out lineToAdd)) break;

                // Unroll 'normal' variables
                //--------------------------
                if (!Rules.ReplaceLineVariables(lineToAdd, out lineToAdd)) break;

                // Unroll shell calls like '$(cmd options)'
                //-----------------------------------------
                if (!Commands.ProcessCommands(lineToAdd, out lineToAdd, parsedMakefile.TextContent, lineNumber)) break;

                // Handle if-clause
                //-----------------
                if (!Rules.CheckIfClause(lineToAdd, out lineToAdd)) break;

                // Store new variable assignments
                //-------------------------------
                if (!Rules.StoreLineVariable(lineToAdd, out lineToAdd)) break;

                // Add phonies
                //------------
                if (!Rules.AddPhonies(lineToAdd, out lineToAdd)) break;

                // Extract Macros
                //---------------
                if (!Rules.ExtractMacros(lineToAdd, out lineToAdd)) break;

                // Extract Targets
                //----------------
                if (!Rules.ExtractTargets(lineToAdd, out lineToAdd)) break;

                // Process includes
                //-----------------
                if (!Rules.ProcessIncludes(lineToAdd, out lineToAdd, parsedMakefile.TextContent, lineNumber, CurrentPath)) break;

                if (lineToAdd == null || lineToAdd.Trim().Length == 0)
                {
                    //makefileContent[lineNumber] = "# " + line + " // REMOVED";
                    parsedMakefile.TextContent.RemoveAt(lineNumber);
                    lineNumber--;
                }
                else if (lineToAdd != line)
                {
                    parsedMakefile.TextContent[lineNumber] = lineToAdd;
                }
            }

            // Check if each line of makefile could be parsed
            //-----------------------------------------------
            if(parsedMakefile.TextContent.Count > 0)
            {
                Console.WriteLine("Makefile contains not parsable content:");
                int lineCount = 0;
                foreach(string line in parsedMakefile.TextContent)
                {
                    if(lineCount > 10)
                    {
                        Console.WriteLine("...");
                        break;
                    }
                    Console.WriteLine(lineCount + ": " + line);
                    lineCount++;
                }
                return false;
            }

            return true;
        }
    }
}
